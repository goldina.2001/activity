package com.goldina.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var editTextLastName: EditText
    private lateinit var editTextName: EditText
    private lateinit var editTextMiddleName: EditText
    private lateinit var editTextAge: EditText
    private lateinit var editTextHobby: EditText
    private lateinit var buttonNext: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonNext=findViewById(R.id.buttonNext)
        editTextLastName=findViewById(R.id.editTextLastName)
        editTextName = findViewById(R.id.editTextName)
        editTextMiddleName = findViewById(R.id.editTextMiddleName)
        editTextAge = findViewById(R.id.editTextAge)
        editTextHobby = findViewById(R.id.editTextHobby)
        buttonNext.setOnClickListener {
            Intent(this@MainActivity, InfoActivity::class.java).also {
                it.putExtra(PersonInfo.lastName, editTextLastName.text.toString())
                it.putExtra(PersonInfo.name, editTextName.text.toString())
                it.putExtra(PersonInfo.middleName, editTextMiddleName.text.toString())
                it.putExtra(PersonInfo.age, editTextAge.text.toString())
                it.putExtra(PersonInfo.descrpHobby, editTextHobby.text.toString())
                startActivity(it)
            }
        }
    }
}